#!/bin/bash

sourcedir="./ascii_daq_sshfs"

# CHANGE RELEVANT PATH HERE!!!
targetdir="sytov@channel.fe.infn.it:/dati/common/GALORE2023/DATA"

echo "I will synchronize " $sourcedir " with " $targetdir

if [ ! -d $sourcedir ]; then
        echo $sourcedir " does not exists"
        exit 1
fi

#if [ ! -d $targetdir ]; then
#        echo $targetdir " does not exists"
#        exit 1
#fi




sleepseconds=10

while true; do
        rsync -avz $sourcedir $targetdir
        echo "I will sleep " $sleepseconds " seconds"
        sleep $sleepseconds
done
