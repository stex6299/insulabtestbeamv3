#!/bin/bash

sourcedir="/eos/project/i/insulab-como/testBeam/TB_2023_06_H8_GALORE/HDF5_PROT/"

# CHANGE RELEVANT PATH HERE!!!
targetdir="/eos/project/i/insulab-como/testBeam/TB_2023_06_H8_GALORE/HDF5"

echo "I will synchronize " $sourcedir " with " $targetdir

if [ ! -d $sourcedir ]; then
        echo $sourcedir " does not exists"
        exit 1
fi

if [ ! -d $targetdir ]; then
        echo $targetdir " does not exists"
        exit 1
fi




sleepseconds=10

while true; do
        rsync -avz $sourcedir $targetdir
        echo "I will sleep " $sleepseconds " seconds"
        sleep $sleepseconds
done
