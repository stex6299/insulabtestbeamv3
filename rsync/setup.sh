#!/bin/bash

asciidir="./ascii_daq_sshfs"
rawdir="./raw_daq_sshfs"

echo "Provo a smontare le cartelle"
fusermount -u $asciidir
fusermount -u $rawdir


# if [ ! -d $asciidir ]; then
#     echo "Creating $asciidir"
    mkdir -p $asciidir
# fi

# if [ ! -d $rawdir ]; then
#     echo "Creating $rawdir"
    mkdir -p $rawdir
# fi

echo "Cleaning $asciidir"
rm -vrf $asciidir/*
echo "Cleaning $rawdir"
rm -vrf $rawdir/*


# CHANGE RELEVANT PATH HERE!!!
echo "Mounting data folder..."
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.151.48:/data/insudaq/ascii_galore2023_h8 $asciidir/ 
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.151.48:/data/insudaq/datadir_galore2023_h8 $rawdir/ 
echo "Mounted!!"
