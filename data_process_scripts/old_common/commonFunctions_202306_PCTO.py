"""
Libreria comune di funzioni da chiamare nei file HDF5.py e ROOT.py,
che processano gli ASCII strippati per produrre i rispettivi formati
"""

# ***** Chiavi del file json *****
asciiPath_key = "asciiPath"
HDF5_key = "HDF5"
numWaveform_key = "numWaveform"
currAscii_key = "currAscii"
currSpill_key = "currSpill"
ROOT_key = "ROOT"



# ***** Import moduli *****
import sys, os, re, glob, time

import numpy as np
import pandas as pd

import json
import h5py
#import uproot

from io import StringIO




# ***** Funzioni *****
def elencaRun(asciiFolder):
    """
    RETURN: elencaRun ritorna una lista con tutti i numeri di run esistenti 
    nella cartella asciiFolder
    
    Funzionamento:
    - Mi faccio elencare tutte le prime spill
    - Splitto al separatore (os.path.sep) 
      Per compatibilità con maligno, mi tengo i due separatori a mano
    - Prendo l'ultima cosa (-1), ovvero il nome del file
    - Dal carattere 3 al 9 c'è il numero di run, converto ad intero
    
    CALL: lstofAllRun = elencaRun(settings[asciiPath_key])
    
    TODO: Magari farla in più righe, essendo una funzione, così diventa più leggibile
    
    """
    
    #lstofAllRun = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(asciiFolder,"*_00001.dat"))]
    lstofAllRun = [int(re.split(os.path.sep, f)[-1][3:9]) for f in glob.glob(os.path.join(asciiFolder,"*_00001.dat"))]
    return lstofAllRun


def elencaSpill(asciiFolder, numRun):
    """
    RETURN: elencaSpill ritorna una lista con tutti i numeri di spill esistenti
    nella cartella asciiFolder per la run numRun
    
    CALL: lstofAllSpill = elencaSpill(settings[asciiPath_key], 501230)
    
    FEATURES:
    

    """
        
    #lstofAllSpill = [int(re.split(r"\\|/", f)[-1].split("_")[-1].split(".")[0]) for f in glob.glob(os.path.join(asciiFolder,f"run{numRun}_*.dat"))]
    lstofAllSpill = [int(re.split(os.path.sep, f)[-1].split("_")[-1].split(".")[0]) for f in glob.glob(os.path.join(asciiFolder,f"run{numRun}_ascii_*.dat"))]
    return lstofAllSpill



def dataframeFromAscii(ASCIIfile, numWF):
    """
    RETURN: dataframeFromAscii ritorna un dataframe a partire da un file ASCII di una run
    
    CALL: df = dataframeFromAscii(ASCIIfile, numWF)
    """


    """
    TODO:
        - Testare con files vuoti, non so dove potrebbe rompersi, ma credo sia
        a posto
    """
    
    # Controllo che il file non sia vuoto
    if os.path.getsize(ASCIIfile) == 0:
        print(f"Il file {ASCIIfile} è vuoto")
        return
    
            
    
    # Se ho delle waveform, apro il file ascii in questione e lo appiattisco
    with open(ASCIIfile, "r") as f:
        
        # Linee del file appiattito
        newtxt = []
        
        # Ciclo sulle linee
        for i,line in enumerate(f):
            if line in ["", "\n"]: continue
            
            # Se sono un multiplo di numWF+1, allora ho finito un blocco 
            if i % (numWF+1) == 0:
                # Se non sono al primo giro, mi appendo la linea precedente
                # con l'eccezione dell'ultimo spazio
                if i!=0:
                    newtxt.append(tmpLine[:-1] + "\n")
                # Replace 4, 3 and 2 spaces into 1
                tmpLine = line.lstrip().replace("\n", " ").replace("    ", " ").replace("   ", " ").replace("  ", " ")
            else:
                tmpLine = tmpLine + line.lstrip().replace("\n", " ").replace("    ", " ").replace("   ", " ").replace("  ", " ")
        
        # Appendo l'ultima riga
        #if i>0:
        newtxt.append(tmpLine[:-1] + "\n")
        
        
        testoAppiattito = "".join(newtxt)
        csvStringIO = StringIO(testoAppiattito)
        # Dovrebbe essere superfluo
        if len(testoAppiattito) == 0: return
        
        return pd.read_csv(csvStringIO, sep=" ", header=None, skiprows=0, )




# Nota: gli ascii pariono iniziare con uno spazio: lo tolgo con lstrip
def scriviDati(HDF5file, ASCIIfile, numWF):
    
    df = dataframeFromAscii(ASCIIfile, numWF,)       

    if df is None: return  


    righeLastFile = len(df.index)
    print(f"Numero di colonne: {len(df.columns)}")
    print(f"Numero di righe: {righeLastFile}")

    
    # Scrivo i dati nel file HDF5
    # Posso aprirlo in append anche se non esiste
    with h5py.File(HDF5file, 'a', libver='latest') as hf:
        print(HDF5file)
    
        hf.swmr_mode = True
		
		
        # Se il file non esisteva
        opts = {"compression":"gzip", "chunks":True}
        
        if (len(hf.keys())) == 0:
            
            
            # 2022_12_Lab Settings
            hf.create_dataset("xpos", data =  df.iloc[:,0:4], maxshape=(None,4), **opts)

            hf.create_dataset("PH1", data =  df.iloc[:,4:12], maxshape=(None,8), **opts)
            hf.create_dataset("PH2", data =  df.iloc[:,12:20], maxshape=(None,8), **opts)
            hf.create_dataset("PH3", data =  df.iloc[:,20:28], maxshape=(None,8), **opts)
            hf.create_dataset("Time1", data =  df.iloc[:,28:36], maxshape=(None,8), **opts)
            hf.create_dataset("Time2", data =  df.iloc[:,36:44], maxshape=(None,8), **opts)
            hf.create_dataset("Time3", data =  df.iloc[:,44:52], maxshape=(None,8), **opts)

            hf.create_dataset("Trun", data =  df.iloc[:,52],  maxshape=(None,), **opts)
            hf.create_dataset("Tunix", data =  df.iloc[:,53],  maxshape=(None,), **opts)
            hf.create_dataset("Ievent", data =  df.iloc[:,54],  maxshape=(None,), **opts)

            """
            #Waveform - Può essere comodo avere qui
            nSamples = 1031
            nStart = 68
            for i in range(numWF):
                hf.create_dataset(f"wf{i}", data =  df.iloc[:,(nStart+nSamples*i):(nStart+nSamples*(i+1))], maxshape=(None,nSamples), **opts)
            """
            
        else:
            
            # ================================================================
            # Provo a capire quant'è la minima shape
            # A volte crasha a metà e, nel crashare, aggiorna solamente alcune colonne
            # Andrà inserito nello script principale....

            tmpMin = np.nan

            for k in hf.keys():
                if np.isnan(tmpMin):
                    tmpMin = hf[k].shape[0]
                else:
                    if hf[k].shape[0] < tmpMin:
                        tmpMin = hf[k].shape[0]

            for k in hf.keys():
                #hf[k].resize((hf[k].shape[0] + righeLastFile), axis = 0)
                hf[k].resize((tmpMin + righeLastFile), axis = 0)

            # In questo modo dovrebbe essere robusto
            # =======================================================
                
               
            

            # 2022_12_Lab Settings
            hf["xpos"][-righeLastFile:] = df.iloc[:,0:4]

            hf["PH1"][-righeLastFile:] = df.iloc[:,4:12]
            hf["PH2"][-righeLastFile:] = df.iloc[:,12:20]
            hf["PH3"][-righeLastFile:] = df.iloc[:,20:28]
            hf["Time1"][-righeLastFile:] = df.iloc[:,28:36]
            hf["Time2"][-righeLastFile:] = df.iloc[:,36:44]
            hf["Time3"][-righeLastFile:] = df.iloc[:,44:52]

            hf["Trun"][-righeLastFile:] = df.iloc[:,52]
            hf["Tunix"][-righeLastFile:] = df.iloc[:,53]
            hf["Ievent"][-righeLastFile:] = df.iloc[:,54]
            """
            #Waveform
            nSamples = 1031
            nStart = 68
            for i in range(numWF):
                hf[f"wf{i}"][-righeLastFile:] = df.iloc[:,(nStart+nSamples*i):(nStart+nSamples*(i+1))]
            """
            

    return





def aggiornaConfig(settings, configFile):
    """
    aggiornaConfig permette di aggiornare il dizionario delle configurazioni.
    Chiamare passando il dizionario stesso. Il file viene invece automaticamente
    pescato dalla funzione.
    
    CALL: aggiornaConfig(settings, configFile)
    """
    # Lo salvo
    try:
        with open(configFile, "w") as f:
            json.dump(settings, f, indent=4)
    except Exception as e: 
        print(e)
        print("Non ho salvato le informazioni nel dizionario, resteranno solo in memoria")
        
    return



def scriviLocalRoot(ROOTfile, ASCIIfile, numWF):
    """
    scriviLocalRoot permette di scrivere un ROOT file locale a partire da un certo ASCII
    
    Non c'è il problema di cancellarlo perché con recreate, già succede
    """
    
    
    """
    Copiata da scriviDati
    """
    
    df = dataframeFromAscii(ASCIIfile, numWF)       
    
    righeLastFile = len(df.index)
    print(f"Numero di colonne: {len(df.columns)}")
    print(f"Numero di righe: {righeLastFile}")
    
    
    
    # Parte nuova: scrivo il root file
    with uproot.recreate(ROOTfile) as f:
        f["t"] = {
            # 2022_12_Lab Settings
            "xpos" : df.iloc[:,0:4],

            "PH1" : df.iloc[:,4:12],
            "PH2" : df.iloc[:,12:20],
            "PH3" : df.iloc[:,20:28],
            "Time1" : df.iloc[:,28:36],
            "Time2" : df.iloc[:,36:44],
            "Time3" : df.iloc[:,44:52],

            "Trun" : df.iloc[:,52],
            "Tunix" : df.iloc[:,53],
            "Ievent" : df.iloc[:,54],
        }
        

    
            

    return

    
