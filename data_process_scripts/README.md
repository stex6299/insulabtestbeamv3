# How to setup conda env
```
conda create -n TBCERN
conda activate TBCERN
conda install -c anaconda hdf5
conda install pip
pip install h5py

# To verify
print(h5py.version.info)
```
