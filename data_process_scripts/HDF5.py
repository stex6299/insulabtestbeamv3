#!/usr/bin/env python3

from commonFunctions import *

# File contenente il dizionario con i settings
configFile = r"./config.json"


# Controllo che esista il file di config
try:
    with open(configFile, "r") as f:
        settings = json.load(f)
except:
    print("Sicuro che esista il file di config?")
    sys.exit(1)
    
    
# Check if folder exist
if not os.path.isdir(settings[asciiPath_key]):
    print(f"Ascii path not valid\t{settings[asciiPath_key]}")
    sys.exit(1)
    
if not os.path.isdir(settings[HDF5_key]):
    print(f"HDF5 path not valid\t{settings[HDF5_key]}")
    sys.exit(1)
    
    
    
    
#%% Big loop

while(True):
    
    # Ottengo la lista di tutte le run esistenti
    lstofAllRun = elencaRun(settings[asciiPath_key])
    
    # Controllo che esista nella directory almeno una run, per consistenza
    # con i pezzi seguenti
    if len(lstofAllRun) == 0:
        time.sleep(10)
        continue
        # wait till there exist at least one ascii file


    # Ottengo la lista delle spill dell'ASCII corrente
    lstofAllSpill = elencaSpill(settings[asciiPath_key], settings[currAscii_key])
    
    
    
    # Se inizializzo bene non dovrebbe essere vitale, ma costa poca fatica lasciarlo
    # e poi è più robusto
    # Direi che è utile per l'inizio se inizializzo da una run bassa a caso
    
    # Se l'ASCII non esiste (e quindi non ha spill, verosimilmente è un pede),
    # passo all'ASCII dopo
    if len(lstofAllSpill) == 0: 
        settings[currAscii_key] += 1
        settings[currSpill_key] = 0
        aggiornaConfig(settings, configFile)
        continue
    
    
    # Se esiste una nuova spill dell'ASCII corrente
    # La scrivo ed incremento il contatore delle spill
    # Se ho un errore in scrittura, semplicemente passo oltre
    if np.array(lstofAllSpill).max() > settings[currSpill_key]:
        try:
            infile = os.path.join(settings[asciiPath_key], f"run{settings[currAscii_key]}_{settings[currSpill_key]+1:06d}.dat")
            outfile = os.path.join(settings[HDF5_key], f"run{settings[currAscii_key]}.h5")
                        
            print(f"\nSto per appendere {infile} a {outfile}")
            scriviDati(outfile, infile, settings[numWaveform_key])
            
            settings[currSpill_key] += 1
            aggiornaConfig(settings, configFile)
            
            continue
        
        except Exception as e: 
            print(e)
            print("Mannaggia, ci riproviamo al prossimo giro")
                        
            time.sleep(1)
            
    # Se non ho nuove spill, ma in compenso esiste un nuovo ascii        
    elif np.array(lstofAllRun).max() > settings[currAscii_key]:
        
        # NOTA PER IL ME DEL FUTURO:
        # In questo preciso punto ci si arriva solo quando ho finito di appendere
        # i dati dell'ultima spill e mi accorgo che esiste la run successiva.
        # è il punto ideale per creare in un colpo solo tree, npz o formati 
        # compressi non appendibili
        # settings[currAscii_key] è il numero della run che mi interessa


        
        # Incremento il contatore
        settings[currAscii_key] += 1
        
        # Incremento finche non esiste la run
        while not(settings[currAscii_key] in lstofAllRun):
            settings[currAscii_key] += 1

        # Imposto la spill a 0 ed incremento il contatore
        settings[currSpill_key] = 0
        aggiornaConfig(settings, configFile)
        
        
        


    time.sleep(10)

