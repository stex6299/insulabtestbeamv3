#!/bin/bash

sshfsdir="./ASCII_SSHFS"

echo "Provo a smontare la cartella"
fusermount -u $sshfsdir

# if [ ! -d $sshfsdir ]; then
#     echo "Creating $sshfsdir"
    mkdir -p $sshfsdir
# fi

echo "Cleaning $sshfsdir"
rm -vrf $sshfsdir/*


# CHANGE RELEVANT PATH HERE!!!
echo "Mounting data folder..."
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.151.48:/data/insudaq/ascii_galore2023_h8 $sshfsdir/ 
echo "Mounted!!"
